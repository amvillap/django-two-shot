from django import forms
from receipts.models import Receipt, ExpenseCategory, Account

class CreateReceipt(forms.ModelForm):
    class Meta:
        model = Receipt
        fields = [
            "vendor",
            "total",
            "tax",
            "date",
            "category",
            "account",
        ]

class NewExpenseCategory(forms.ModelForm):
    class Meta:
        model = ExpenseCategory
        fields = [
            "name",
        ]

class NewAccount(forms.ModelForm):
    class Meta:
        model = Account
        fields = [
            "name",
            "number",
        ]
