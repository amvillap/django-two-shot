from django.shortcuts import render, redirect
from receipts.models import Receipt, ExpenseCategory, Account
from django.contrib.auth.decorators import login_required
from receipts.forms import CreateReceipt, NewExpenseCategory, NewAccount


# Create your views here.
@login_required(login_url="login")
def receipt_instance(request):
    receipt_instance = Receipt.objects.filter(purchaser=request.user)
    context = {"receipt_instance": receipt_instance}
    return render(request, "receipts/home.html", context)

@login_required(login_url="login")
def create_receipt(request):
    if request.method == "POST":
        form = CreateReceipt(request.POST)
        if form.is_valid():
            receipt = form.save(False)
            receipt.purchaser= request.user
            receipt.save()
            return redirect("home")
    else:
        form = CreateReceipt()
        context = {
            "createreceipt_form": form,
        }
        return render(request, "receipts/create.html", context)

def show_categories(request):
    category_list = ExpenseCategory.objects.all()
    context = {
        "category_list": category_list
    }
    return render(request, "receipts/categories.html",context)

def show_accounts(request):
    accounts = Account.objects.all()
    context = {
        "accounts_list": accounts
    }
    return render(request, "receipts/accounts.html", context)

@login_required(login_url="login")
def create_category(request):
    if request.method == "POST":
        form = NewExpenseCategory(request.POST)
        if form.is_valid():
            new_category = form.save(False)
            new_category.owner = request.user
            new_category.save()
            return redirect("category_list")
    else:
        form = NewExpenseCategory()
        context = {
            "newcategory_form":form
        }
        return render(request,"receipts/newcategory.html", context)

@login_required(login_url="login")
def create_account(request):
    if request.method == "POST":
        form = NewAccount(request.POST)
        if form.is_valid():
            newaccount = form.save(False)
            newaccount.owner = request.user
            newaccount.save()
            return redirect("account_list")
    else:
        form = NewAccount()
        context = {
            "newaccount_form":form
        }
        return render(request, "receipts/newaccount.html", context)
